from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import Testimonial, Gallery, Enquiry, ContactDetails, Service, Banner, WhyWorkWithUs


admin.site.register(Testimonial, admin.ModelAdmin)
admin.site.register(Gallery, admin.ModelAdmin)
admin.site.register(ContactDetails, SingletonModelAdmin)
admin.site.register(Banner, SingletonModelAdmin)
admin.site.register(Enquiry, admin.ModelAdmin)
admin.site.register(WhyWorkWithUs, admin.ModelAdmin)
admin.site.register(Service, admin.ModelAdmin)
