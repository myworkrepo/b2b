from django import forms

from .models import Enquiry


class ContactForm(forms.ModelForm):

    class Meta:
        model = Enquiry
        fields = ('first_name', 'last_name', 'subject', 'email', 'message')
