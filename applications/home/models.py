from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField
from solo.models import SingletonModel


class Banner(SingletonModel):
    title = models.CharField(max_length=255, null=True, blank=True)
    description = RichTextUploadingField()
    logo = models.ImageField(null=True, blank=True)
    background_image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Banner"


class WhyWorkWithUs(models.Model):
    title = models.CharField(max_length=255)
    description = RichTextUploadingField()
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Why Work With Us"
        verbose_name_plural = "Why Work With Us"


class Service(models.Model):
    image = models.ImageField(null=True, blank=True)
    title = models.CharField(max_length=255, null=True, blank=True)
    description = RichTextUploadingField(null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Service"
        verbose_name_plural = "Services"


class ContactDetails(SingletonModel):
    phone = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    address = RichTextUploadingField(null=True, blank=True)
    facebook = models.URLField(null=True, blank=True)
    twitter = models.URLField(null=True, blank=True)
    youtube = models.URLField(null=True, blank=True)
    instagram = models.URLField(null=True, blank=True)

    def __str__(self):
        return 'Contact Details'

    class Meta:
        verbose_name = "Contact Details"


class Testimonial(models.Model):
    author = models.CharField(max_length=255)
    testimonial = models.TextField()
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.author

    class Meta:
        verbose_name = "Testimonial"
        verbose_name_plural = "Testimonials"


class Gallery(models.Model):
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = "Gallery"
        verbose_name_plural = "Gallery"


class Enquiry(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    subject = models.CharField(max_length=255)
    email = models.EmailField()
    message = models.TextField()

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Enquiry'
        verbose_name_plural = 'Enquiries'
