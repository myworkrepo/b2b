import json

from django.views import generic
from django.http import HttpResponse
from django.http import JsonResponse
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from .forms import ContactForm
from .models import Testimonial, Gallery, Enquiry, ContactDetails, Service, Banner, WhyWorkWithUs



class HomeView(generic.TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['contact'] = ContactDetails.get_solo()
        context['banner'] = Banner.get_solo()
        context['services'] = Service.objects.all()
        context['why'] = WhyWorkWithUs.objects.all()[:3]
        context['testimonials'] = Testimonial.objects.all()
        context['galleries'] = Gallery.objects.all()
        return context


class ContactView(generic.FormView):

    form_class = ContactForm
    template_name = "contact.html"

    def form_valid(self, form):
        text_template = get_template('email_templates/contact_message.html')
        user_name = form.cleaned_data['first_name']
        subject = 'Contact request from {}'.format(user_name)
        context = {'formentry': self.request.POST}
        text_content = text_template.render(context)
        from_email = settings.EMAIL_HOST_USER
        email = ContactDetails.get_solo().email
        to = [email]
        msg = EmailMultiAlternatives(subject, text_content, from_email, to)
        msg.attach_alternative(text_content, "text/html")
        msg.send()
        form.save()
        return JsonResponse({'status':'success'})

    def form_invalid(self, form):
        print(form.errors)
        return HttpResponse(json.dumps({'result': {'status': 'form_error', 'errors': form.errors}}),
                            content_type='application/json')
